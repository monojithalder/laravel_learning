<html>
<head>
  <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container  col-md-offset-3" style="margin-top:10px;">
  <ul class="nav nav-pills" role="tablist">
    <li><a href="http://localhost/laravel/public/member/show_member">Show Member</a></li>
    <li><a href="http://localhost/laravel/public/member/registration">Registration</a></li>
    @if ($data['login'] == FALSE)
    <li><a href="http://localhost/laravel/public/login">Login</a></li>
    @endif
    @if ($data['login'] == TRUE)
      <li><a href="http://localhost/laravel/public/logout">Logout</a></li>
    @endif
  </ul>
  <div class="form-horizontal">
    <div class="form-group col-md-6" style="margin-top:20px;">
      <div class="control">
            @yield('content')
      </div>

        </div>

  </div>

</div>
</body>
</html>
