
@extends('master')
@section('content')

{!! Form::open(['url' => 'member/insert_member','method' => 'GET']) !!}

  {!! Form::text('name',null,['class' => 'form-control input-xlarge']) !!}
  {!! Form::text('username',null,['class' => 'form-control input-xlarge']) !!}
  {!! Form::password('password', array('class' => 'form-control input-xlarge')) !!}
  {!! Form::text('phone',null,['class' => 'form-control input-xlarge']) !!}
  {!! Form::textarea('address',null,['class' => 'form-control input-xlarge']) !!}
  {!! Form::submit('Register',array('class' => 'btn btn-danger')) !!}
   @foreach ($errors->all() as $error)
        <span class="help-block">{{ $error }}</span>
    @endforeach
{!! Form::close()!!}

@stop
