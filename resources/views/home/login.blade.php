@extends('master')
@section('content')

{!! Form::open(['url' => '/do_login','method' => 'POST']) !!}

  {!! Form::text('username',null,['class' => 'form-control input-xlarge']) !!}
  {!! Form::password('password', array('class' => 'form-control input-xlarge')) !!}
  {!! Form::submit('Register',array('class' => 'btn btn-danger')) !!}
{!! Form::close()!!}

@stop
