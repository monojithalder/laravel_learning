@extends('master')
@section('content')
  <table  class="table table-striped">
    <thead>
      <tr>
          <td>Id</td>
          <td>Name</td>
          <td>UserName</td>
          <td>Password</td>
          <td>Phone</td>
          <td>Address</td>
          <td>Edit</td>
          <td>Delete</td>

      </tr>
    </thead>
    <tbody>
      @foreach ($member as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->username }}</td>
            <td>{{ $value->password }}</td>
            <td>{{ $value->phone }}</td>
            <td>{{ $value->address }}</td>
            <td><a href="http://localhost/laravel/public/member/edit/{{ $value->id }}">Edit</a></td>
            <td><a href="http://localhost/laravel/public/member/delete_member/{{ $value->id }}">Delete</a></td>
        </tr>
      @endforeach
    </tbody>
  </table>
@stop
