@extends('master')
@section('content')

{!! Form::open(['url' => 'update_info_table/'.$user->id, 'method' => 'PATCH']) !!}
  {!! Form::text('id',$user['id'],['class' => 'form-control input-xlarge']) !!}
  {!! Form::text('name',$user['name'],['class' => 'form-control input-xlarge']) !!}
  {!! Form::text('username',$user['username'],['class' => 'form-control input-xlarge']) !!}
  {!! Form::text('password',$user['password'],['class' => 'form-control input-xlarge']) !!}
  {!! Form::text('phone',$details['phone'],['class' => 'form-control input-xlarge']) !!}
  {!! Form::textarea('address',$details['address'],['class' => 'form-control input-xlarge']) !!}
  {!! Form::submit('Save Member',array('class' => 'btn btn-danger')) !!}
{!! Form::close() !!}


@stop
