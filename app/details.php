<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class details extends Eloquent
{
    protected $table = 'details';
    public $timestamps = false;
    protected $fillable = [
      'member_id','phone','address'
    ];
}
