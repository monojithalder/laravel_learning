<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\info;
use App\details;
use Session;
use App\Http\Requests\CreateMemberRequest;

class Home extends Controller
{
    //
    private $details;
    public function __construct(details $details) {
      $this->details = $details;
    }

    public function index()
    {
      $data['frameworks'] = array();
      $data['frameworks'][0] = 'CodeIgniter';
      $data['frameworks'][1] = 'Laravel';
      $data['frameworks'][2] = 'CakePHP';
      $data['frameworks'][3] = 'Yii2';
      return view('home.index',$data);
    }

    public function about()
    {
        $data = array();
        $data['name'] = 'Monojit Halder';

        return view('home.about',$data);
    }

    public function show_id($username,info $user_info)
    {
      $user = $user_info->where('username','=',$username)->first();
      //$user = DB::table('user')->where('id', '>', 1)->get();
      $user_info->password = 'hello';
      $user_info->username = 'test';
     // foreach ($user as $key => $value) {
          //return $value->id.'    '.$value->username;
      //}
      //$user_info->save();
      return $user->id . '    '.$user->username.'    '.$user->pasword;

    }

    public function get_from_db()
    {
        $users = DB::table('info')->get();
        return view('home.db',compact('users'));
    }

    public function update_info_table($id,Request $request,info $user_info)
    {
      $user = $user_info->where('id','=',$id)->first();
      $user->username = $request->get('username');
      $user->password = $request->get('password');
      $user->name     = $request->get('name');
      $user->save();
      $other = $this->details->where('member_id','=',$id)->first();
      if($other)
      {
        $other->phone   = $request->get('phone');
        $other->address = $request->get('address');
        $other->save();
      }
      else {
        $this->details->fill(['member_id' => $id,
          'phone' => $request->get('phone'),
          'address' => $request->get('address')])->save();
      }

      //$user_info->fill(['username' => $request->get('username')]);
      //return $request->get('phone');
      return redirect('member/show_member');
    }

    public function show_edit_form($id,info $user_info)
    {
      $data = array();
      $data['user'] = $user_info->where('id','=',$id)->first();
      $data['details'] = $this->details->where('member_id','=',$id)->first();
      return view('home.edit_member',$data);
    }

    public function member_registration()
    {
      $data['login'] = Session::get('login');
       return view('home.member_registration',compact('data'));
    }

    public function member_insert(info $user_info,CreateMemberRequest $request) {
      $user_info->fill(['username' => $request->get('username'),
        'password' => $request->get('password'),
        'name' => $request->get('name')])->save();
      $member_id = $user_info->id;
      $this->details->fill(['member_id' => $member_id,
        'phone' => $request->get('phone'),
        'address' => $request->get('address')
        ])->save();
      return redirect('member/show_member');
    }

    public function show_member(info $user_info)
    {
      //$member = $user_info->get();
     /* $member = $user_info->join('details','info.id','=',
        'details.member_id')->select('info.id as id','info.name as name',
        'info.username as username','details.phone','details.address');
      */
      $login = Session::get('login');
      if($login)
      {
        $data['login'] = Session::get('login');
       $member = DB::table('info')
              ->leftjoin('details', 'info.id', '=', 'details.member_id')
              ->select('info.id', 'info.name', 'info.username',
                'info.password',
                'details.phone','details.address')
              ->get();
        return view('home.show_member',compact('member'),compact('data'));
      }
      else
      {
        return redirect('login');
      }
    }

    public function delete_member($id,info $user_info)
    {
       $user_info->where('id','=',$id)->delete();
        return redirect('member/show_member');
    }

    public function login()
    {
        $data['login'] = Session::get('login');
        return view('home.login',compact('data'));
    }

    public function do_login(info $info,Request $request)
    {
      $username = $request->get('username');
      $password = $request->get('password');

        $user_info = $info->where(
          'username','=', $username
          )->where('password','=',$password)
        ->first();

        if($user_info) {
          Session::put('login',TRUE);
          return redirect('myaccount');
        }
        else {
          return redirect('login');
        }
    }

    public function myaccount()
    {
        $login = Session::get('login');
        if(!$login)
        {
            return redirect('login');
        }
        else
        {
            $data['login'] = $login;
            return view('home.myaccount',compact('data'));
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('login');
    }
}
