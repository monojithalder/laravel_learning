<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    //return view('Hello Word');
    print('Hello Word');
});*/
Route::get('/','Home@index');
Route::get('about','Home@about');
Route::get('find/{username}','Home@show_id');
Route::get('/db','Home@get_from_db');
Route::get('/member/edit/{id}','Home@show_edit_form');
Route::patch('update_info_table/{id}','Home@update_info_table');
get('/member/registration','Home@member_registration');
get('member/insert_member','Home@member_insert');
get('member/show_member','Home@show_member');
get('member/delete_member/{id}','Home@delete_member');
get('login','Home@login');
post('do_login','Home@do_login');
get('myaccount','Home@myaccount');
get('logout','Home@logout');
