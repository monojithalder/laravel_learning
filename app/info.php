<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class info extends Eloquent
{
    protected $table = 'info';
    public $timestamps = false;
    protected $fillable = [
      'name','username','password'
    ];
}
